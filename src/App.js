import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginPage from './pages/Login';
import RegisterPage from './pages/Register';
import AdminDashboard from './pages/AdminDashboard';
import UserProducts from './pages/UserProducts'
import './App.css';

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/admin" element={<AdminDashboard />} /> {}
          <Route path="/products" element={<UserProducts />} /> {}

          {/* add more routes here for your other pages */}
        </Routes>
      </div>
    </Router>
  );
}

export default App;