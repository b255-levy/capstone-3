import React, { useEffect, useState } from 'react';

const UserProducts = () => {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);
  const [showInactive, setShowInactive] = useState(false);

  useEffect(() => {
    fetch('http://localhost:4000/users/products', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        return response.json();
      })
      .then((data) => {
        if (Array.isArray(data)) {
          setProducts(data);
        } else {
          console.error('Unexpected data structure:', data);
        }
      })
      .catch((error) => {
        console.error('Fetch error:', error);
      });
  }, []);

  const handleAddToCart = async (product) => {
    const token = localStorage.getItem('authToken');
    try {
      const response = await fetch('http://localhost:4000/users/checkout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({
          cart: [{ productId: product._id }],
        }),
      });
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }
      const data = await response.json();
      setCart(data.cart); // Update the cart state with the new cart data from the server
    } catch (error) {
      console.error('Add to cart error:', error);
    }
  };

  const checkout = async () => {
    const token = localStorage.getItem('authToken');
    try {
      const response = await fetch('http://localhost:4000/users/checkout', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify({ cart }),
      });

      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      }

      alert('Checkout successful!'); // Notify user
      setCart([]); // Clear cart after successful checkout
    } catch (error) {
      console.error('Checkout error:', error);
      alert('Checkout failed!'); // Notify user
    }
  };

  const handleToggleInactive = () => {
    setShowInactive(!showInactive);
  };

  return (
    <div>
      <button onClick={handleToggleInactive}>
        {showInactive ? 'Hide Inactive Products' : 'Show Inactive Products'}
      </button>
      {products
        .filter((product) => showInactive || product.isActive)
        .map((product) => (
          <div key={product._id}>
            <h2>{product.name}</h2>
            <p>{product.description}</p>
            <p>{product.price}</p>
            <button onClick={() => handleAddToCart(product)}>Add to Cart</button>
          </div>
        ))}
      <button onClick={checkout}>Checkout</button> {/* Checkout button */}
    </div>
  );
};

export default UserProducts;
